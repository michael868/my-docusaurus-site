Section 1 Introduction
======================

# Purpose

>Direction: Use the paragraphs provided below.

This VSA describes the vision, scope and proposed architecture for the <<System>>. The purpose of the VSA is to provide repository references to the analysis and resulting outputs needed to bound the scope and characterize the system architecture solution for a given Product Planning Review (PRR, under Agile) exit.
In addition, the VSA provides a framework for verifying consistency between the vision and scoped capabilities or epics, and the proposed high-level architecture. This is done by Business and Technical Lead consensus. Mechanically, the primary traceability and check is that the proposed system architecture demonstrates support for each of the actor events, inputs and outputs as shown in the high-level architecture depiction called for in the VSA template.

>The following text is for guidance purposes and need not be duplicated in the project’s VSA

Projects should follow REPO and EA guidance on scoping the effort leveraging the highest level of definitions necessary to support “sufficient architecture to begin design” and determine that the envisioned effort is “within acceptable risks”.
A sufficiently bounded scope for purposes of streamlined scope and architecture definition is one that:
Provides the vision and capability information called for in this template
States the high-level Capabilities (or Epics) to be implemented
Elaborates these Capabilities (or Epics) as defined in this template (e.g., through models, user stories, visualizations, etc.) to the degree necessary to characterize the system architecture solution within acceptable risk, enough to begin design.
Sufficient architecture to begin design means:
Provides the major architectural components and patterns called for in this template, as validated by the Technical Lead,
Enterprise component allocations from the Project Charter are represented in the system architecture context or traced to by one or more of those components in the system architecture context (see subsection 1.4.1). The Technical Lead is responsible for verification that all Enterprise component allocations are accounted for in the manner just described.
Within acceptable risk means:
The level of risk that significant architectural refactoring will be needed or that major technical barriers will be encountered during design and development, are both within acceptable limits as adjudicated by the Technical Lead, with concurrence from the Business Lead, meaning the Technical Lead has conveyed technical risks to the Business Lead to the extent necessary for the Business Lead to evaluate the potential business impact. >>

# VSA Scope

>Direction: Use the paragraph below that applies.

This VSA specifies <<pick which of the following two paragraphs applies and use that one>> 
…the overall objectives, vision, scope, and system architecture for the <<System Name>> for the identified period.
<<Or…>>
… updates to the VSA for <<system>> to specify the release plan for the next release, Release <<XX>>, and any updates to the objectives, scope or system architecture resulting from changes occurring during execution of the last release, and team and management reflection subsequent to its completion.

# Vision, Scope and Architecture Organization

>Direction: Provide a brief summary statement of each section and appendices. Modify the following text if tailored.

This VSA has the following sections and appendix:
Section 1:         Introduction — describes the purpose, scope, organization, maintenance, and accessibility of the VSA
Section 2:         References — specifies project assets and other references closely related to or referenced by the VSA
Section 3:         Vision — describes the purpose, objectives and future state vision for the system objective
Section 4:         Scope — provides any functionality (e.g., epics) and decomposition needed to scope and bound the system
Section 5:         System Architecture — characterizes the system architecture sufficient to begin design within acceptable risk
Appendix A:     Acronyms and Glossary — provides explanations for abbreviations and acronyms used in the VSA.
Appendix B:     Technical Debt Attachments — captures any completed TD Assessment to attest that the solution does not contain a TD, and/or a TD Profile to provide sufficient information to enable governance to make an informed decision about the proposed technical debt.

# VSA Maintenance
 
>Direction: Use the following text.

If there are any changes to the VSA content (through CR) during sprinting that extends the bounds of the defined scope or alters the exiting scope in a way that redefines the system architecture, the VSA must be updated and receive final approval as part of the Integration Readiness Review (IRR) in the Agile Path. Changes between PPR and IRR that affect the scope or architecture of the system must be approved through the applicable change management process. A condition of approval for any such change is that all impacted architecture specifications and traceability are fully updated in their authoritative source repository(ies), along with maintenance of change history, and the current capabilities and architecture must be available for viewing on demand by authorized roles. A new baseline must be created. A current, complete baseline will be established prior to beginning any new release.

## Maintenance of VSA Artifacts and Traceability

>Direction: It is vital that scope and architecture specifications & outputs be kept in a repository at PPR and updated/refined during any Agile process. This includes any context diagram and system flow depicting the system architecture. Furthermore, design components developed between PPR and IRR need to trace to appropriate architecture component in the system architecture.

If the System Architect, Rational Collaborative Lifecycle Management (CLM) suite, or other enterprise approved tool is one of the repositories used, the specific steps that follow must be taken to ensure the referenced artifacts in that repository are baselined for the VSA, and readily accessible to appropriate reviewers:
Make a request to the repository owner to baseline the current workspace for VSA.
Ensure that anyone who needs access to review these models has access to the architecture repository, including any client version, as needed.
Where the reference to the authoritative model(s) in the repository is placed, provide:
The name of the baselined workspace created for the VSA
The name(s) and model element type(s) of the referenced artifact(s)

# VSA Distribution

>Include the following

This section identifies the tools, and their location, to be used to create, manage and provide traceability between PPR system architecture and design level components:

>Identify tools here

The Project Manager and Technical Lead further certify that maintenance and validation of ongoing traceability between system architecture and design components will be maintained, included as the part of the definition of DONE for any iteration, and validated at each iteration.

>Direction: Identify who will have access to the VSA and how it will be provided. Describe any limitations on access or use. Be sure to include a description of who will be responsible for ensuring all reviewers have necessary and timely access to referenced repositories, and who will establish and monitor this access.


